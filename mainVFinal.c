#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>


int main()

{
    int answer, x, y, goods;
    srand(time(NULL));

    int code = rand() % 999;
    int z = 0;
    char hot;



    printf("Welcome to...\n\n\n");
    sleep(1);

    printf("THE CAVE\n\n");
    printf("Press any key to begin your quest:");
    getch();
    system("cls"); // Loading Segment
    for (y = 0; y < 2; y++) {
            for (x = 0; x < 8; x++){
                switch(x) {
                case 0: printf("L "); break;
                case 1: printf("O "); break;
                case 2: printf("A "); break;
                case 3: printf("D "); break;
                case 4: printf("I "); break;
                case 5: printf("N "); break;
                case 6: printf("G "); break;
                case 7: system("cls"); break;
            }
            usleep(100*1000);
    }
    }

    printf("The bravest warrior in the land, aka, you,\nhas been sent on a quest to kill an Ogre\nthat has plagued the land for months.\n\n\n");
    sleep(1);
    printf("Although you've been the 7th or so 'bravest' warrior.\nYou are confident in your abilities\nand are sure that you can destroy the beast.\n");
    sleep(1);
    printf("\nPress any key to continue\n\n");
    getch();
    system("cls");

    printf("After a few days of traveling you have reached the cave.\nExhausted, you decide to rest before you enter.");
    sleep(1);
    printf("\n\nUnfortunately, when you wake, you notice that your\nsword, shield, and helm are missing.\nHearing a faint clatter from the save you surmise that whatever took your\ngear was most likely in the cave.\n\n");
    sleep(1);
    printf("What do you do?:\n1. Proceed into the cave\n2. Be a pussy and leave.\n");
    scanf("%d", &answer);
    system("cls");
    if (answer == 2){ // Decision 1
        printf("You are the worst warrior in the land,\nand are executed upon return to the castle for being a bitch.");
        exit(0);
    } else {
        printf("You go into the cave.");
    }

    printf("\n\n5 minutes in and you notice something extremely shiny on top of a boulder\na few yards away. It seems to be your helm! What do you do?");
    printf("\n1. Take the helm\n2. Search around some more.\n\n");
    scanf("%d", &x);
    switch(x) { // Decision 2


        case 1: printf("\nYou pick up the helm quickly expecting something to stop you\nbut nothing happens, and you easily take back your first piece.");
        goods++;
        printf("\nPress any key to continue\n\n");
        getch();
        system("cls");
        break;

        default: printf("\nSearching around a bit longer yields some runes that appear to say: %d\n", code);
        sleep(1);
        printf("Nothing else seems out of order, take the helm?\n1. Yes\n2. No\n");
        scanf("%d", &x);
        system("cls");
        switch(x){ // Search case
        case 1: printf("\nYou pick up the helm quickly expecting something to stop you\nbut nothing happens, and you easily take back your first piece.");
                goods++;
                printf("\nPress any key to continue\n\n");
                getch();
                system("cls");
                break;

                default: printf("\nYou decide against taking the helm.\nAs it could easily still be booby trapped.\n");
                printf("\nPress any key to continue\n\n");
                getch();
                system("cls");
                break;
                }
                break;

    }




    printf("You continue walking along until come across a door\nAfter examining it you find a hidden description that reads\n'Enter the code' with a pitch black rock shaped pen next to it");
    sleep(1);
    printf("\nYou decide to take your best guess at the code:\n"); // Puzzle 1
    scanf("%d", &x);
    system("cls");
    if (x == code){
        printf("Right after you finish scratching in the last number,\nthe door opens, revealing the path ahead.");
    } else {
        printf("After a short pause, the cave begins to shake\nBoulders fall from the roof and crush you.\nYOU DIED");
        exit(0);
    }
    printf("\nHours pass, and you begin to tire\nYou decide to take a rest.\n\n");
    printf("\nPress any key to continue\n\n");
    getch();
    system("cls");



    printf("CRASH!\n\n'WAKE UP, or I'll kill you where you lie', you hear something shriek\n");
    printf("You quickly get up see an excited goblin.\n\n'Aha!', it said, 'About damn time.'\n");
    sleep(1);
    printf("'HEY, if you answer a question correctly, I'll give you back this',\n it gestures to the shield.\n'What do you say? Refuse to play or answer wrong and you're grind meat'");
    printf("\n1. Okay... 2. Hell no\n"); //Decision 3
        scanf("%d", &x);
        system("cls");
        switch(x){ // Search case
                case 2: printf("Angry, you charge the goblin and attempt to grab the shield\nCackling it dodges you and breaks your knees with your own shield in one swift movement.\n'More meat for the Underlord I suppose', it says laughing.\nYOU DIED");
                    exit(0);
                    break;


                default:printf("You agree to the challenge"); // Puzzle 2
                        printf("\n'What' it says, 'could 4.65 be in C?'\n");
                        printf("1. 'an int' 2. 'a float' 3. 'a string'\n");
                        scanf("%d", &answer);
                        system("cls");
                        switch(answer){ // Search case
                        case 2: printf("\nThe goblin scowls, 'Blast this new educational system!'.\nIt drops the shield and runs off. Easy.\n");
                            goods++;
                            printf("\nPress any key to continue\n\n");
                            getch();
                            system("cls");
                            break;

                        default: printf("'Wrong!', the goblin screeches.\nIt quickly jumps up and bites your neck, spraying your blood everywhere.\nYOU DIED");
                            exit(0);
                            break;
                            }
                        break;
                        }




                printf("A stench rots the air, as you continue forward\nit gets ever more unbearable.\n\nWhen you can barely withstand it anymore, you finally see the monster\n");
                sleep(1);
                printf("Bigger and definitely smellier than you imagined, \n the ogre rises and sizes you up\n");
                sleep(1);
                printf("\n'SMALL, WEAK', it yells.\nYou notice your sword, unfortunately in the hands of the ogre himself\n'I'LL STICK YOU WITH YOUR OWN BLADE', it laughs\n");
                printf("What do you do?\n1. Try to reason with it. 2. Attempt to headbutt.\n3. Charge him with your shield. 4. Throw your shield at the stalactites hanging above.\n\n"); //Decision 4

                scanf("%d", &x);
                if (x == 1){
                    printf("Your attempts to reason horribly fail\nThe ogre easily slices you in two\nYOU DIED");
                    exit(0);
                } else if (x == 4) {
                            if (goods > 1){ //Checks for gear player collects
                            printf("You throw your shield at the ceiling and \nyou notice new cracks on some of the rock formations.\nWhat now?\n");
                            printf("1. Throw your helm at the rocks. 2. Run.\n");
                            scanf("%d", &x);
                            system("cls");
                                switch(x){
                                    case 1: printf("In a last ditch effort\nthe spear-like rocks on top of the cave fall\nA large one splits the ogres head in two.\nYou return two days later to the kingdom with the ogre's finger as proof\nof your victory.\n\nYOU WIN\nTHANKS FOR PLAYING");
                                    // Win Statement
                                    break;
                                    default: printf("You are the worst warrior in the land,\nand are executed upon return to the castle for being a bitch.");
                                    exit(0);
                                    break;
                                }
                            } else {
                            printf("You almost broke through, but it was not enough, the ogre flattens you.\nYOU DIED");
                            exit(0);
                            }
                    } else {
                        printf("The ogre easily deflects your attack, and breaks you in two.\nYOU DIED");
                       exit(0);
                    }
                    return(0);
}
